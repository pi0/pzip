#include <iostream>
#include "../PZIP/PZip.h"
#include "../PZIP/FileReader.h"
#include "../PZIP/FileWriter.h"

int printUsage();
void msg(const char*, PZipMessageType);
void msg2(const char* msg);
void prer(int p);


int main(int argc, char** args){

	using namespace std;

	if (argc != 4)
		return printUsage();
	
	if (args[1][0]=='c') {
		PZip z(new FileReader(args[2]), PZipMode::Compress);
		z.notifier.message_listeners.push_back(msg);
		z.notifier.status_listeners.push_back(msg2);
		z.notifier.progress_listeners.push_back(prer);
		z.compress(new FileWriter(args[3]));
	}
	else if (args[1][0] == 'x') {
		PZip z(new FileReader(args[2]), PZipMode::Decompress);
		z.notifier.message_listeners.push_back(msg);
		z.notifier.status_listeners.push_back(msg2);
		z.notifier.progress_listeners.push_back(prer);
		z.decompress(new FileWriter(args[3]));
	}
	else {
		cout << "Invalid mode " << args[1][0];
		return printUsage();
	}
	
	return 0;
}

bool msg_out = true;

void msg(const char* msg, PZipMessageType type){
	const char* tg = (type == PZipMessageType::Info) ? "[Info]" : "[Error]";
	std::cout << tg << msg << std::endl;
	msg_out = true;
}

void msg2(const char* a) {
	msg(a, PZipMessageType::Info);
}


void prer(int p) {
	if (!msg_out)
		std::cout << "\r";
	msg_out = false;
	std::cout << p << "%\r";
}


int printUsage() {
	using namespace std;
	cout << "PZIP" << endl;
	cout << "Usage pzip c|x src dst";
	return 0;
}