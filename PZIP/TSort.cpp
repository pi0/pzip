#pragma once
#define TSortCPP

#include "TSort.h"

namespace _SortInternals {

    template<typename T>
    void insertionSort(T *data, int count, int (*comp)(T &a, T &b), bool ascending) {

        int r = ascending ? +1 : -1;

        for (int i = 0; i < count; i++) {
            T curr = data[i];
            int j = i;
            for (; j > 0 && comp(data[j - 1], curr) == r; j--)
                data[j] = data[j - 1];
            data[j] = curr;
        }

    }

}

template<typename T>
void sort(T* data,int count,int (*comp)(T& a,T& b),SortType type,bool ascending) {
    switch (type) {
        case SortType::Insertion:
            _SortInternals::insertionSort(data, count, comp, ascending);
            break;
        default:
            //TODO : throw not supported
            break;
    }
}
