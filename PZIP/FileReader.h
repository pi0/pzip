#pragma once

#include "IReader.h"
#include <fstream>
#include <string>

class FileReader : public IReader {

private:
    std::ifstream _file;

    bool _eof = false;
    char buff = 0;
    int buff_read_bits = 0;
	long file_size = 0;

    void buffer();

public:
    FileReader(const std::string &path);

    virtual unsigned char readChar();

    virtual bool readBit();

    virtual int readInt();

    virtual bool eof();

	virtual double pos();

	virtual long getSize();

    virtual void seek(int pos, SeekType type);
};