#pragma once

#include <string>
#include "IWriter.h"

class PZipNode {
public:

    char chr = 0;
    int frequency = 0;
    PZipNode *l_c = nullptr, *r_c = nullptr;

    PZipNode(char chr, int frequency = 0);

    PZipNode(PZipNode *l_c, PZipNode *r_c);

    ~PZipNode();

    static int compare(PZipNode *&a, PZipNode *&b);

    std::string toString();

    bool isLeaf();

    int getSize();

    int countLeaf();

    void print(const std::string &prefix = "");

    void save(IWriter *out);
};