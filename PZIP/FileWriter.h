#include "IWriter.h"
#include <fstream>
#include <string>

class FileWriter : public IWriter {

private:
    std::ofstream _file;
    char bit_buff=0;
    int bit_buff_count=0;

    void flush_bit_buff();


public:
    FileWriter(const std::string &path);

    ~FileWriter(){
        flush();
		//_file.close();
    }

    virtual void writeChar(char ch);

    virtual void writeBit(bool bit);

    virtual void writeInt(int a) override;

    virtual void flush();
};


