#include "PZipNotifier.h"


PZipNotifier::PZipNotifier()
{
}


PZipNotifier::~PZipNotifier()
{
}


void PZipNotifier::updateProgress(int p) {
	for (auto f = progress_listeners.begin(); f != progress_listeners.end(); f++)
		(*f)(p);
}


void PZipNotifier::updateStatus(const char* status) {
	for (auto f = status_listeners.begin(); f != status_listeners.end(); f++)
		(*f)(status);
}


void PZipNotifier::message(const char* msg, PZipMessageType type) {
	for (auto f = message_listeners.begin(); f != message_listeners.end(); f++)
		(*f)(msg,type);
}
