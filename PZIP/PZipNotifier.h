#pragma once

#include <string>
#include <vector>
#include <functional>

enum PZipMessageType {
	Info, Error
};

class PZipNotifier
{
private:

public:
	PZipNotifier();
	~PZipNotifier();

	typedef std::function<void(int)> progress_listener_func;
	std::vector < progress_listener_func > progress_listeners;
	void updateProgress(int p);

	typedef std::function<void(const char*)> status_listener_func;
	std::vector < status_listener_func > status_listeners;
	void updateStatus(const char* status);

	typedef std::function<void(const char*, PZipMessageType)> message_listener_func;
	std::vector < message_listener_func > message_listeners;
	void message(const char*, PZipMessageType type = PZipMessageType::Info);

};

