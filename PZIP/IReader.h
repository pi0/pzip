#pragma once

class IReader {

public:
    enum SeekType {
        beg, curr, end
    };

    virtual unsigned char readChar() = 0;

    virtual bool readBit() = 0;

    virtual int readInt() = 0;

    virtual bool eof() = 0;

    virtual void seek(int pos, SeekType type = IReader::SeekType::beg) = 0;

	virtual double pos() = 0;

    char *readStr(int count) {
        char *r = new char[count + 1];
        for (int i = 0; i < count; i++)
            r[i] = readChar();
        r[count] = 0;
        return r;
    }
};
