#pragma once


enum SortType {
    Insertion
};

template<typename T>
void sort(T *data, int count, int (*comp)(T &a, T &b), SortType type, bool ascending = true);

#ifndef TSortCPP
#include "TSort.cpp"
#endif // !TSortCPP