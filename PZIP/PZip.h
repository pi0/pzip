#pragma once

#include "IReader.h"
#include "PZipTree.h"
#include "IWriter.h"
#include "PZipTreeIO.h"
#include "PZipNotifier.h"
#include <string>

enum PZipMode{
    Compress,Decompress
};


class PZip {

private:
    PZipMode  mode;
    IReader *data;
    PZipTree *tree;
    PZipTreeIO *treeIO;
    int totalchars=0;
	bool is_initialized = false;

public:

	PZipNotifier notifier = PZipNotifier();

    PZip(IReader *data,PZipMode mode);

    ~PZip();

	void init();

    void compress(IWriter *out);

    void decompress(IWriter *out);


    PZipMode const &getMode() const {
        return mode;
    }

    PZipTree *getTree() const {
        return tree;
	}

	PZipTreeIO *getIOTree() const {
		return treeIO;
	}

    void print();
};

