#include "FileReader.h"

FileReader::FileReader(const std::string &path) {
	_file.open(path, std::ios::binary);
    _eof = !_file.is_open();
	if (_eof){
		//TODO : throw something !
		return;
	}

	//Get file size
	_file.seekg(0, std::ios_base::end);
	file_size = _file.tellg();
	_file.clear();
	_file.seekg(0, std::ios_base::beg);

    buffer();
}

void FileReader::buffer() {
    if (!_eof)
        _file.get(buff);
    else
        buff = 0;
    buff_read_bits = 0;
    _eof = _file.eof();
}

unsigned char FileReader::readChar() {
    unsigned char c;
    if (buff_read_bits == 0) {
        c = (unsigned char) buff;
        buffer();
    } else {
        c = 0;
        for (int i = 0; i < 8; i++)
            if (readBit())
                c |= 1 << 7 - i;
    }
    return c;
}

bool FileReader::readBit() {
    if (buff_read_bits == 8)
        buffer();
    bool r = (bool) (buff & 1 << 7 - buff_read_bits);
    buff_read_bits++;
    return r;
}

int FileReader::readInt() {
    int r=0;
    r|= (int)readChar()<<24;
    r|= (int)readChar()<<16;
    r|= (int)readChar()<<8;
    r|= (int)readChar();
    return r;
}

bool FileReader::eof() {
    return _eof;
}

double FileReader::pos(){
	return (_file.tellg()*1.0) / file_size;
}

long FileReader::getSize(){
	return file_size;
}

void FileReader::seek(int pos, IReader::SeekType type) {
    std::ios_base::seekdir dir = std::ios_base::beg;
    switch (type) {
        case IReader::SeekType::beg:
            dir = std::ios_base::beg;
            break;
        case IReader::SeekType::curr:
            dir = std::ios_base::cur;
            break;
        case IReader::SeekType::end:
			dir = std::ios_base::end;
            break;
    }
    _file.clear();//Clear EOF state BEFORE seekg()
    _eof = false;
    _file.seekg(pos, dir);
    buffer();//Start new buffer
}


