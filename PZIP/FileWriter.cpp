#include "FileWriter.h"

FileWriter::FileWriter(const std::string &path) {
	_file.open(path, std::ios::binary);
}

void FileWriter::writeChar(char ch) {
    if (bit_buff_count == 0)
        _file << ch;
    else
        for (int i = 0; i < 8; i++)
            writeBit((bool) (ch & 1 << 7 - i));
}

void FileWriter::writeBit(bool bit) {
    if (bit_buff_count == 8)
        flush_bit_buff();
    if(bit)
        bit_buff |= 1 << 7 - bit_buff_count;
    else
        bit_buff &= ~(1 << 7 - bit_buff_count);
    bit_buff_count++;
}

void FileWriter::flush_bit_buff() {
    if(bit_buff_count) {
        _file << bit_buff;
        bit_buff = 0;
        bit_buff_count = 0;
    }
}

void FileWriter::flush() {
    flush_bit_buff();
    _file.flush();
}


void FileWriter::writeInt(int a) {
    writeChar((char) (a>>24 & 0xFF));
    writeChar((char) (a>>16 & 0xFF));
    writeChar((char) (a>>8 & 0xFF));
    writeChar((char) (a & 0xFF));
}
