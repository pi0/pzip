#include "PZipTree.h"


class PZipTreeIO {

private:
    PZipTree *tree;

    void addToBST(PZipNode *node, int value, int len);

	class BSTNode;
    void printBST(BSTNode *root, std::string prefix);

public:

	class BSTNode {
	public:
		PZipNode *node;
		BSTNode *l = nullptr, *r = nullptr;
		int value;
		int value_len;


		BSTNode(PZipNode *node, int value, int value_len) : node(node), value(value), value_len(value_len) {
		}

		~BSTNode() {
			if (l)delete l;
			if (r)delete r;
		}

		std::string toString();

	} *broot = nullptr;

    PZipTreeIO(PZipTree *tree);

    void setTree(PZipTree *tree);

    char decode(IReader *reader, IWriter *writer = nullptr);

    void encode(char data, IWriter *writer);

	BSTNode* getRoot() const{
		return broot;
	}

    void printBST() {
        printBST(broot, "");
    }
};