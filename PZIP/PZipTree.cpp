#include <iostream>
#include "PZipTree.h"
#include "TSort.h"

PZipTree::PZipTree(PZipNode *root) : root(root) {

}

PZipTree *PZipTree::buildTree(IReader *reader, int* num_of_chars, PZipNotifier* notifier) {
    //Init all possible nodes
    PZipNode **nodes = new PZipNode *[256];
    for (int i = 0; i < 256; i++)
        nodes[i] = new PZipNode((char) i);

    //Count each node frequency
    int count = 0;
	if (num_of_chars)
		*num_of_chars = 0;
    while (!reader->eof()) {
        unsigned char c = reader->readChar();

        if (!nodes[c]->frequency++)
            count++;
        if(num_of_chars)
            *num_of_chars+=1;
		if (notifier){
			int p = (int) (100 * reader->pos());
			if (p%10==0)
				notifier->updateProgress(p);
		}
    }

    //We need at least 2 nodes ... Keep something !!
    if (count < 1) {
        if (!nodes[0]->frequency)
            nodes[0]->frequency++;
        else nodes[1]->frequency++;
        count++;
    }

    //Remove empty nodes
    PZipNode **tmp = new PZipNode *[count];
    int j = 0;
    for (int i = 0; i < 256; i++)
        if (nodes[i]->frequency)
            tmp[j++] = nodes[i];
    delete[] nodes;
    nodes = tmp;

    //Sort by frequency
    sort(nodes, count, PZipNode::compare, SortType::Insertion, false/*descending*/);

    //Build huffman tree
    for (int c = count - 1; c > 0; c--) {
        //Merge
        PZipNode *n = new PZipNode(nodes[c - 1], nodes[c]);
        nodes[c - 1] = n;
        //Resort last element
        int i;
        for (i = c - 1; i > 0 && PZipNode::compare(nodes[i - 1], n) == -1; i--)
            nodes[i] = nodes[i - 1];
        nodes[i] = n;
    }

    return new PZipTree(nodes[0]);
}


void PZipTree::saveTree(IWriter *out) {
    if (root)
        root->save(out);
}

PZipNode *readNode(IReader *reader) {
    if (reader->readBit()) //Leaf
//    if (reader->readChar() == '1') //Leaf
        return new PZipNode(reader->readChar(), 1);
    // Else
    PZipNode *l = readNode(reader);
    PZipNode *r = readNode(reader);
    return new PZipNode(l, r);
}


PZipTree *PZipTree::loadTree(IReader *reader) {
    PZipNode *root = readNode(reader);
    return new PZipTree(root);
}


int PZipTree::getDataSize() {
    if (!root)
        return 0;
    return root->countLeaf();
}

void PZipTree::print() {
    if (root)
        root->print();
}
