#include "PZipNode.h"
#include <sstream>
#include <iostream>


PZipNode::PZipNode(char chr, int frequency) : chr(chr), frequency(frequency) {

}

PZipNode::PZipNode(PZipNode *l_c, PZipNode *r_c) : l_c(l_c), r_c(r_c) {
    frequency = l_c->frequency + r_c->frequency;
}

PZipNode::~PZipNode() {

}

int PZipNode::compare(PZipNode *&a, PZipNode *&b) {
    if (a->frequency > b->frequency)
        return +1;
    else if (a->frequency < b->frequency)
        return -1;
    else
        return 0;
}

int PZipNode::getSize() {
    int s = 1;
    if (l_c)
        s += l_c->getSize();
    if (r_c)
        s += r_c->getSize();
    return s;
}

bool PZipNode::isLeaf() {
    return !l_c && !r_c;
}

int PZipNode::countLeaf() {
    if (isLeaf())
        return 1;
    int s = 0;
    if (l_c)
        s += l_c->countLeaf();
    if (r_c)
        s += r_c->countLeaf();
    return s;
}

void getSubNodes(PZipNode *n, std::stringstream &s) {
    if(!n)
        return;;
    if(n->isLeaf())
        s<<n->toString();
    else {
        getSubNodes(n->l_c, s);
        getSubNodes(n->r_c, s);
    }
}

std::string PZipNode::toString() {
    std::stringstream s;
    if (isLeaf()) {
        if(chr>32 && chr<127)//Then is should be readable enough :)
            s << chr;
        else
            s << "\\"<<(unsigned char)chr+0;
    } else {
        s<<"[";
        getSubNodes(this, s);
        s<<"]";
    }
    return s.str();
}


void PZipNode::print(std::string const &prefix) {
    std::cout << prefix + (isLeaf()?" - ":" + ") <<toString() << std::endl;
    if (l_c)
        l_c->print(prefix + "   ");
    if (r_c)
        r_c->print(prefix + "   ");
}

void PZipNode::save(IWriter *out) {
    if (isLeaf()) {
        out->writeBit(1);
//        out->writeChar('1');
        out->writeChar(chr);
    } else {
        out->writeBit(0);
//        out->writeChar('0');
        if (l_c)
            l_c->save(out);
        if (r_c)
            r_c->save(out);
    }
}
