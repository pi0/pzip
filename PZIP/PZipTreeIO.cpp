#include <iostream>
#include <sstream>

#include "PZipTreeIO.h"

PZipTreeIO::PZipTreeIO(PZipTree *tree) {
    setTree(tree);
}


void PZipTreeIO::setTree(PZipTree *tree) {
    PZipTreeIO::tree = tree;
    if (broot) {
        delete broot;
        broot = nullptr;
    }
    addToBST(tree->getRoot(), 0, 0);
}

void PZipTreeIO::addToBST(PZipNode *node, int value, int len) {
    if (!node)
        return;

    if (!node->isLeaf()) {
        if (node->l_c)
            addToBST(node->l_c, value, len + 1);
        if (node->r_c)
            addToBST(node->r_c, value|1<<31-len, len + 1);
        return;
    }

    //We have reached to a leaf node
    BSTNode *n = new BSTNode(node,value,len);
    if (!broot)
        broot = n;
    else {
        BSTNode *parent = broot;
        while (parent) {
            BSTNode *next = nullptr;//This prevents adding/overriding duplicate values
            if (node->chr < parent->node->chr) {
                next = parent->l;
                if (!next)
                    parent->l = n;
            } else if (node->chr > parent->node->chr) {
                next = parent->r;
                if (!next)
                    parent->r = n;
            } //else {
            //Duplicate value
            //}
            parent = next;
        }
    }
}


void PZipTreeIO::encode(char data, IWriter* writer) {
    //Find data in BST
    BSTNode* n=broot;
    while(n){
        if(data<n->node->chr)
            n=n->l;
        else if(data>n->node->chr)
            n=n->r;
        else {
            //Found
            for(int i=0;i<n->value_len;i++)
                writer->writeBit((bool) (n->value & 1 << 31 - i));
//               writer->writeChar((bool) (n->value & 1 << 31 - i)?'1':'0');
            return;
        }
    }
    //TODO : throw not in db
}

char PZipTreeIO::decode(IReader* reader,IWriter* writer) {
    PZipNode* n=tree->getRoot();
    while(n && !n->isLeaf()){
        if(reader->readBit())
//        if(reader->readChar()=='1')
            n=n->r_c;
        else
            n=n->l_c;
    }
    if(n && n->isLeaf() && !reader->eof()) {
        if(writer)
            writer->writeChar(n->chr);
        return n->chr;
    }else
        return 0;
}


std::string PZipTreeIO::BSTNode::toString() {
	std::stringstream s;
	s<<node->toString() << " = ";
	for (int i = 0; i<value_len; i++)
		s << (bool)(value & 1 << 31 - i);
	return s.str();
}

void PZipTreeIO::printBST(PZipTreeIO::BSTNode *root, std::string prefix) {
    if(!root)
        return;
	std::cout <</*prefix*/"" << root->toString()<<std::endl;
    printBST(root->l, prefix+" ");
    printBST(root->r, prefix+" ");

}
