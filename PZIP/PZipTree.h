#pragma once

#include "IReader.h"
#include "PZipNode.h"
#include "IWriter.h"
#include "PZipNotifier.h"

class PZipTree {

private:
    PZipNode *root = nullptr;

    PZipTree(PZipNode *root);

public:
    static PZipTree *buildTree(IReader *reader,int* num_of_chars= nullptr,PZipNotifier* notifier=nullptr);

    static PZipTree *loadTree(IReader *reader);

    void saveTree(IWriter *out);

    int getDataSize();

    void print();

    PZipNode *getRoot() const{
        return root;
    }
};