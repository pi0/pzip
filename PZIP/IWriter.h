#pragma once

class IWriter
{

public:
    virtual void writeChar(char ch) = 0;
    virtual void writeBit(bool bit)=0;
    virtual void writeStr(const char* str){
        for(int i=0;str[i];i++)
            writeChar(str[i]);
    }
    virtual void writeInt(int a)=0;
    virtual void flush()=0;
};
