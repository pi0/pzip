#include <iostream>
#include <string.h>
#include "PZip.h"

PZip::PZip(IReader *data, PZipMode mode) : data(data), mode(mode) {
	
}


PZip::~PZip() {
    delete tree;
    delete treeIO;
}

void PZip::init() {
	if (is_initialized)return;
	is_initialized = true;

	if (mode == PZipMode::Compress) {
		//Build the tree
		notifier.updateStatus("Building index...");
		data->seek(0);
		tree = PZipTree::buildTree(data, &totalchars, &notifier);
	}
	else {
		//Load Metadata and Tree from file
		notifier.updateStatus("Loading index...");
		char *head = data->readStr(2);
		if (strcmp(head, "PZ")) {
			//TODO : throw invalid format
			return;
		}
		delete[] head;
		tree = PZipTree::loadTree(data);
		totalchars = data->readInt();
	}
	notifier.updateStatus("Building IO Tree ...");
	//Now construct a Searcher
	treeIO = new PZipTreeIO(tree);
	notifier.updateStatus("Loaded !");
}

void PZip::compress(IWriter *out) {
	init();
    if (mode != PZipMode::Compress) {
        //TODO : throw invalid operation
        return;
    }
    out->writeStr("PZ");
    tree->saveTree(out);
    out->writeInt(totalchars);
    data->seek(0);
    for(int r=totalchars;r>0 && !data->eof();r--)
        treeIO->encode(data->readChar(), out);
    out->flush();
}

void PZip::decompress(IWriter *out) {
	init();
    if (mode != PZipMode::Decompress) {
        //TODO : throw invalid operation
        return;
    }

    for(int r=totalchars;r>0 && !data->eof();r--)
        treeIO->decode(data,out);

    out->flush();
}

void PZip::print() {
    std::cout << "Mode : " << (mode == PZipMode::Compress ? "Compress" : "Decompress") << std::endl;
    std::cout << "Index size : " << tree->getDataSize() << std::endl;
    tree->print();
    std::cout<<"BST :"<<std::endl;
    treeIO->printBST();
}
