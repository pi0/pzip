
// PZIP GUIDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "../PZIP/PZip.h"

// CPZIPGUIDlg dialog
class CPZIPGUIDlg : public CDialogEx
{
// Construction
public:
	CPZIPGUIDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_PZIPGUI_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP();

public:

	PZip* pz=nullptr;

	void setFilename(const CString &name);
	void loadFile();
	void start(bool compress);
	void set_progress(int p);
	void set_status(const char* msg);
	void load_tree();

	afx_msg void OnBnClickedbrowse();
	CString file_name;
	afx_msg void OnBnClickedunzip();
	CString cstr_status;
	CProgressCtrl progress_bar;
	afx_msg void OnBnClickedzip();
	CTreeCtrl tree_huffman;

	CTreeCtrl tree_bst;
	afx_msg void OnStnClickedlblstatus();
};
