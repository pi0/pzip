
// PZIP GUI.h : main header file for the PZIP application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CPZIPGUIApp:
// See PZIP GUI.cpp for the implementation of this class
//

class CPZIPGUIApp : public CWinApp
{
public:
	CPZIPGUIApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CPZIPGUIApp theApp;