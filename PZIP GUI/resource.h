//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PZIP GUI.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PZIPGUI_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_EDIT1                       1004
#define btn_browse                      1005
#define btn_unzip                       1006
#define btn_zip                         1007
#define btn_tree                        1008
#define btn_exit                        1009
#define btn_about                       1010
#define lblStatus                       1011
#define progbar                         1012
#define editbox_file                    1013
#define btn_cancel                      1014
#define btn_cancel2                     1015
#define id_tree_bst                     1015
#define id_tree_huffman                 1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
