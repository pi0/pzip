
// PZIP GUIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PZIP GUI.h"
#include "PZIP GUIDlg.h"
#include "afxdialogex.h"

#include <string>
#include <functional>

#include "../PZIP/PZip.h"
#include "../PZIP/FileReader.h"
#include "../PZIP/FileWriter.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CPZIPGUIDlg dialog



CPZIPGUIDlg::CPZIPGUIDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPZIPGUIDlg::IDD, pParent)
	, file_name(_T(""))
	, cstr_status(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPZIPGUIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, editbox_file, file_name);
	DDX_Text(pDX, lblStatus, cstr_status);
	DDX_Control(pDX, progbar, progress_bar);
	DDX_Control(pDX, id_tree_huffman, tree_huffman);

	DDX_Control(pDX, id_tree_bst, tree_bst);
}

BEGIN_MESSAGE_MAP(CPZIPGUIDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(btn_browse, &CPZIPGUIDlg::OnBnClickedbrowse)
	ON_BN_CLICKED(btn_unzip, &CPZIPGUIDlg::OnBnClickedunzip)
	ON_BN_CLICKED(btn_zip, &CPZIPGUIDlg::OnBnClickedzip)
	ON_STN_CLICKED(lblStatus, &CPZIPGUIDlg::OnStnClickedlblstatus)
END_MESSAGE_MAP()


// CPZIPGUIDlg message handlers

BOOL CPZIPGUIDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here



	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPZIPGUIDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPZIPGUIDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPZIPGUIDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//======================================================================================================


void CPZIPGUIDlg::OnBnClickedbrowse()
{
	
	CFileDialog fileDlg(TRUE, _T("All Files"), _T("*.*"),
		OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, _T("PZIP Files (*.pz)|*.pz|All Files (*.*)|*.*||"));

	if (fileDlg.DoModal() == IDOK)
	{
		CString pathName = fileDlg.GetPathName();
		setFilename(pathName);
	}

}

void DoEvents()
{
	MSG msg;
	if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void CPZIPGUIDlg::setFilename(const CString &name) {
	UpdateData();
	file_name = name;
	UpdateData(0);
	loadFile();
}

void CPZIPGUIDlg::set_progress(int p){
	if (p > 100)
		p = 0;
	bool ui_lock = (p != 0) || !pz;

	if (ui_lock){
		GetDlgItem(btn_zip)->EnableWindow(false);
		GetDlgItem(btn_unzip)->EnableWindow(false);
	}

	GetDlgItem(btn_cancel)->EnableWindow(ui_lock);

	UpdateData();
	progress_bar.SetPos(p);
	UpdateData(0);

	DoEvents();
}

void CPZIPGUIDlg::set_status(const char* msg){
	UpdateData();
	cstr_status = CString(msg);
	UpdateData(0);
	DoEvents();
}

struct loader_params{
	CString* file_name;
	CPZIPGUIDlg* dialog;
};

UINT loaderThread(LPVOID param){

	struct loader_params* params = (loader_params*)param;

	FileReader *file = new FileReader(std::string(CT2CA(*params->file_name)));
	bool decompress = params->file_name->Right(3) == ".pz";
	params->dialog->pz = new PZip(file, decompress ?
		PZipMode::Decompress : PZipMode::Compress);

	//Progress Listener
	params->dialog->pz->notifier.progress_listeners.push_back(
		std::bind(&CPZIPGUIDlg::set_progress, params->dialog, std::placeholders::_1));

	//Status
	params->dialog->pz->notifier.status_listeners.push_back(
		std::bind(&CPZIPGUIDlg::set_status, params->dialog, std::placeholders::_1));

	params->dialog->pz->init();
	params->dialog->pz->notifier.updateProgress(0);

	params->dialog->GetDlgItem(btn_unzip)->EnableWindow(decompress);
	params->dialog->GetDlgItem(btn_zip)->EnableWindow(!decompress);

	params->dialog->load_tree();

	return 0;
}

struct worker_params{
	PZip * pz;
	bool compress;
	std::string* filename;
};

UINT workerThread(LPVOID param) {
	struct worker_params* params = (worker_params*)param;
	FileWriter w = FileWriter(*params->filename);
	if (params->compress)
		params->pz->compress(&w);
	else
		params->pz->decompress(&w);
	return 0;
}

void CPZIPGUIDlg::loadFile() {
	loader_params *p=new loader_params();
	p->file_name = &file_name;
	p->dialog = this;
	loaderThread(p);
	//AfxBeginThread(loaderThread,p);
}


void huffman_add(CTreeCtrl* tree, HTREEITEM ref, PZipNode* node){
	if (!node) return;
	ref=tree->InsertItem(CString(node->toString().c_str()),ref);
	huffman_add(tree,ref,node->l_c);
	huffman_add(tree,ref,node->r_c);
}

void bst_add(CTreeCtrl* tree, HTREEITEM ref, PZipTreeIO::BSTNode* node){
	if (!node) return;
	ref = tree->InsertItem(CString(node->toString().c_str()), ref);
	bst_add(tree, ref, node->l);
	bst_add(tree, ref, node->r);
}

void CPZIPGUIDlg::load_tree() {
	if (!pz)
		return;
	
	tree_huffman.DeleteAllItems();
	huffman_add(&tree_huffman, nullptr, pz->getTree()->getRoot());
	
	tree_bst.DeleteAllItems();
	bst_add(&tree_bst, nullptr, pz->getIOTree()->getRoot());

}


void CPZIPGUIDlg::start(bool compress) {

	CString name = PathFindFileName(file_name);
	if (compress)
		name += CString(".pz");
	else
		name.Replace(_T(".pz"), _T(""));
	
	CFileDialog fileDlg(FALSE,0, name);

	if (!fileDlg.DoModal() == IDOK)
		return;
	

	worker_params *p = new worker_params();
	p->pz = pz;
	p->compress = compress;
	p->filename = new std::string(CT2A(fileDlg.GetPathName()));
	workerThread(p);
	//AfxBeginThread(workerThread,p);

	setFilename(fileDlg.GetPathName());
}


void CPZIPGUIDlg::OnBnClickedzip()
{
	start(true);
}

void CPZIPGUIDlg::OnBnClickedunzip()
{
	start(false);
}





void CPZIPGUIDlg::OnStnClickedlblstatus()
{
	// TODO: Add your control notification handler code here
}
